import wikipediaapi as wikiapi
import sys
import os.path
import pickle
import requests.exceptions

wiki = wikiapi.Wikipedia(language = 'en')

def print_categorymembers(categorymembers, level=0, max_level=10):
        for c in categorymembers.values():
            print("%s: %s (ns: %d)" % ("*" * (level + 1), c.title, c.ns))
            if c.ns == wikiapi.Namespace.CATEGORY and level < max_level:
                print_categorymembers(c.categorymembers, level=level + 1, max_level=max_level)

def compute_lazily(filename, f, *args):
    if os.path.isfile(filename):
        return pickle.load(open(filename, 'rb')), True

    r = f(*args)
    pickle.dump(r, open(filename, 'wb'))

    return r, False

def get_all_children_pages_from_category_members(category_members, ns):
    stack = [ c for c in category_members.values() ] 
    S = set()
    while stack:
        c = stack.pop()

        if c.ns in ns and c not in S:
            could_request = False
            while not could_request:
                try:
                    S.add(c)
                    slen = len(S)
                    if slen%1000 == 0:
                        print('number of children pages found: ', slen)

                    if c.ns == wikiapi.Namespace.CATEGORY:
                        for i in c.categorymembers.values():
                            stack.append(i)

                    could_request = True
                except requests.exceptions.ReadTimeout:
                    print('read timeout, trying again...', file = sys.stderr)
    return S

def get_all_children_pages(page, ns):
    S = get_all_children_pages_from_category_members(page.categorymembers, ns)
    S.add(page)
    return S

def main():

    music_organizations_page = wiki.page('Category:Music organizations')
    # music_organizations_page = wiki.page('Category:Noise rock groups')

    print('fetching music organizations music organizations page...')
    music_organizations_pages, lazy = compute_lazily(
        'cache/music-organizations-pages.set',
        get_all_children_pages,
        music_organizations_page,
        { wikiapi.Namespace.CATEGORY, wikiapi.Namespace.MAIN }
    )
    print('' if not lazy else 'using previous result!\n', end='')
    # print(music_organizations_pages)

if __name__ == '__main__':
    main()
