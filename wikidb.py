import sqlite3
import os.path

import wikipediaapi as wikiapi
import re

def regexp(expr, item):
    reg = re.compile(expr)
    return reg.search(item) is not None

connection = sqlite3.connect('wiki.db')

connection.create_function("REGEXP", 2, regexp)

cursor = connection.cursor()

cursor.execute(
'''CREATE TABLE IF NOT EXISTS pages(
    id INT PRIMARY KEY NOT NULL,
    title_name TEXT NOT NULL,
    ns INT NOT NULL);''')

cursor.execute(
'''CREATE TABLE IF NOT EXISTS page_has_link_to_page(
    page_from     INT NOT NULL,
    page_to       INT NOT NULL,
    FOREIGN KEY (page_from) REFERENCES PAGE(id),
    FOREIGN KEY (page_to) REFERENCES PAGE(id));''')

cursor.execute(
'''CREATE TABLE IF NOT EXISTS page_has_child(
    page_parent     INT NOT NULL,
    page_child      INT NOT NULL,
    FOREIGN KEY (page_parent) REFERENCES PAGE(id),
    FOREIGN KEY (page_child) REFERENCES PAGE(id));''')

cursor.execute(
'''CREATE TABLE IF NOT EXISTS page_has_parent(
    page_child      INT NOT NULL,
    page_parent     INT NOT NULL,
    FOREIGN KEY (page_child) REFERENCES PAGE(id),
    FOREIGN KEY (page_parent) REFERENCES PAGE(id));''')

def commit():
    connection.commit()

def add(page):
    cursor.execute(
        "INSERT INTO pages (id, title_name, ns) VALUES (?, ?, ?)",
        (page.pageid, page.title, page.ns)
    )

def silently_add(page):
    try:
        add(page)
    except sqlite3.IntegrityError:
        pass

def add_parenthood(parent_page, child_page):
    cursor.execute(
        "INSERT INTO page_has_child (page_parent, page_child) VALUES (?, ?)",
        (parent_page.pageid, child_page.pageid)
    )
    cursor.execute(
        "INSERT INTO page_has_parent (page_child, page_parent) VALUES (?, ?)",
        (child_page.pageid, parent_page.pageid)
    )

def n_pages():
    n, = cursor.execute(
        "SELECT COUNT(*) FROM pages"
    )
    return n[0]

def n_main_pages():
    n, = cursor.execute(
        "SELECT COUNT(*) FROM pages where pages.ns = 0"
    )
    return n[0]

def n_unlinked_pages():
    n, = cursor.execute(
        "SELECT COUNT(*)"
        + " FROM pages LEFT JOIN page_has_link_to_page AS l"
        + " ON pages.id = l.page_from"
        + " WHERE pages.ns = 0 and l.page_from IS NULL"
    )
    return n[0]

def has(page):
    n, = cursor.execute(
        "SELECT COUNT(1) FROM pages WHERE id = ?", (page.pageid,)
    )
    return n[0] > 0

def get_pages():
    cursor.execute('SELECT * FROM pages')
    return cursor.fetchall()

def get_main_pages():
    cursor.execute('SELECT * FROM pages WHERE ns = 0')
    return cursor.fetchall()

def get_unlinked_pages():
    cursor.execute(
        "SELECT *"
        + " FROM pages LEFT JOIN page_has_link_to_page AS l"
        + " ON pages.id = l.page_from"
        + " WHERE pages.ns = 0 and l.page_from IS NULL"
    )
    return cursor.fetchall()


def add_link(f, t):
    cursor.execute(
        "INSERT INTO page_has_link_to_page (page_from, page_to) VALUES (?, ?)",
        (f.pageid, t.pageid)
    )

