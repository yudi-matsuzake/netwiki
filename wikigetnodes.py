import wikipediaapi as wikiapi
import sys
import requests.exceptions
import wikidb

wiki = wikiapi.Wikipedia(language = 'en')

def fetch_all_children_pages(page, ns):
    stack = [ (page, None) ]
    for c in page.categorymembers.values():
        stack.append((c, page))

    parent_control = None

    count = 1

    while stack:
        c, parent = stack.pop()

        if parent != None and parent != parent_control:
            parent_control = parent
            wikidb.commit()

        if c.ns in ns and not wikidb.has(c):
            could_request = False
            while not could_request:
                try:
                    wikidb.add(c)

                    if parent != None:
                        wikidb.add_parenthood(parent, c)

                    # wikidb.commit()

                    if count%1000 == 0:
                        slen = wikidb.n_pages()
                        print('number of children pages found: ', slen)

                    if c.ns == wikiapi.Namespace.CATEGORY:
                        for i in c.categorymembers.values():
                            stack.append((i, c))

                    count += 1
                    could_request = True
                except requests.exceptions.ReadTimeout:
                    print('read timeout, trying again...', file = sys.stderr)

    wikidb.commit()


def main():

    # page_title = 'Category:Music organizations'
    # page_title = 'Category:Noise rock groups'
    page_title = 'Category:Musicians'
    page = wiki.page(page_title)

    print('fetching "{}" page...'.format(page_title))
    fetch_all_children_pages(
        page,
        { wikiapi.Namespace.CATEGORY, wikiapi.Namespace.MAIN }
    )

if __name__ == '__main__':
    main()
