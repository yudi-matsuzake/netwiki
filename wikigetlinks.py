import multiprocessing as mp

import wikidb
import wikipediaapi as wikiapi
import requests
import json
import time
import random
import sys

wiki = wikiapi.Wikipedia(language = 'en')

def get_pages():
    for row in wikidb.get_pages():
        yield wiki.page(row[1])

def get_main_pages():
    for row in wikidb.get_main_pages():
        yield wiki.page(row[1])

def get_unlinked_pages():
    for row in wikidb.get_unlinked_pages():
        yield wiki.page(row[1])

def get_ids():
    for row in wikidb.get_main_pages():
        yield row[0]

page_ids = set(get_ids())

def create_links(page):
    could_request = False
    while not could_request:
        try:
            L = []
            for title, page_link in page.links.items():
                if page_link.ns == wikiapi.Namespace.MAIN and page_link.pageid in page_ids:
                    L.append((page, page_link))
            could_request = True
            return L
        except requests.exceptions.ReadTimeout:
            print('read timeout, trying again...', file = sys.stderr)
        except json.decoder.JSONDecodeError:
            print('decode error', file = sys.stderr)
            print('sleeping', file = sys.stderr)
            time.sleep(random.randint(5, 10))
        except requests.exceptions.SSLError:
            print('ssl error', file = sys.stderr)
            print('sleeping...', file = sys.stderr)
            time.sleep(random.randint(500, 1000))

def process_links(links):
    print('process_links started')
    n = wikidb.n_pages()
    count = 0
    try:
        while True:
            p0, p1 = links.get()
            wikidb.add_link(p0, p1)

            count += 1

            if count % 100 == 0:
                wikidb.commit()
                print(count, ' links made')

    except ValueError:
        print('queue is closed, exiting')

# def save_links(page):
#     could_request = False
#     while not could_request:
#         try:
#             for title, page_link in page.links.items():
#                 if page_link.ns == wikiapi.Namespace.MAIN and wikidb.has(page_link):
#                     print('making link: ', page.title, ' -> ', page_link.title)
#                     wikidb.add_link(page, page_link)
#                     wikidb.commit()
#             could_request = True
#         except requests.exceptions.ReadTimeout:
#             print('read timeout, trying again...', file = sys.stderr)

def main():
    n = wikidb.n_unlinked_pages()
    count = 0

    with mp.Pool(16) as p:
        for links in p.imap(create_links, [ page for page in get_unlinked_pages() ]):
            count += 1
            print('[{:.2%}] {} / {}'.format(float(count)/n, count, n))

            for p0, p1 in links:
                wikidb.add_link(p0, p1)
            wikidb.commit()

if __name__ == '__main__':
    main()
