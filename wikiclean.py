import wikidb


print('deleting all songs')
wikidb.cursor.execute('DELETE FROM pages WHERE title_name REGEXP ?', [ '\(.*\\b[Ss]ong\\b.*\)' ])

print('deleting all albuns')
wikidb.cursor.execute('DELETE FROM pages WHERE title_name REGEXP ?', [ '\(.*\\b[Aa]lbum\\b.*\)' ])

print('deleting all lives')
wikidb.cursor.execute('DELETE FROM pages WHERE title_name LIKE ?', [ "Live in%" ])

print('deleting all lists')
wikidb.cursor.execute('DELETE FROM pages WHERE title_name LIKE ?', [ "List of%" ])

print('deleting all discography')
wikidb.cursor.execute('DELETE FROM pages WHERE title_name LIKE ?', [ '%discography' ])

wikidb.commit()
